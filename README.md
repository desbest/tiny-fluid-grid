# Tiny Fluid Grid

The happy & awesome way to build fluid grid based websites.

Inspired by [1kbgrid.com](http://desbest.com/1kbgrid) [[gitlab]](http://desbest.com/1kbgrid) Developed with love by [Girlfriend's](https://archive.is/x2eqg) overconfident and pretty good looking team of web developers. Tiny Fluid Grid ships with a index.html with demo code, and the grid.css containing the CSS for the grid you created.

The successor to 1KB Grid and Tiny Fluid Grid is [Semantic Grid System](http://desbest.com/semantic-grid-system). 


## FAQ

**What do the numbers in the file name mean?**

They refer to number of columns, column width and gutter width.
