<?php
if ($_GET['gutter_percentage'] == 40 && $_GET['grid_columns'] == 12 && $_GET['min_width'] == 1080 && $_GET['max_width'] == 1600){
echo "
.grid_1 { width: 5%; }
.grid_2 { width: 13.333333333333%; }
.grid_3 { width: 21.666666666667%; }
.grid_4 { width: 30%; }
.grid_5 { width: 38.333333333333%; }
.grid_6 { width: 46.666666666667%; }
.grid_7 { width: 55%; }
.grid_8 { width: 63.333333333333%; }
.grid_9 { width: 71.666666666667%; }
.grid_10 { width: 80%; }
.grid_11 { width: 88.333333333333%; }
.grid_12 { width: 96.666666666667%; }

.grid_1,
.grid_2,
.grid_3,
.grid_4,
.grid_5,
.grid_6,
.grid_7,
.grid_8,
.grid_9,
.grid_10,
.grid_11,
.grid_12 {
	margin-left: 1.6666666666667%;
	margin-right: 1.6666666666667%;
	float: left;
	display: block;
}

.alpha{margin-left:0px;}
.omega{margin-right:0px;}

.container{
	min-width: 1080px; 
	max-width: 1600px; 
	margin: auto;
}

/* @
 * tinyfluidgrid.com
 & girlfriendnyc.com
 */

.clear{clear:both;display:block;overflow:hidden;visibility:hidden;width:0;height:0}.clearfix:after{clear:both;content:' ';display:block;font-size:0;line-height:0;visibility:hidden;width:0;height:0}* html .clearfix,*:first-child+html .clearfix{zoom:1}
";
}
?>