/*
 & tinyfluidgrid
 & Girlfriend NYC
 */

var lasers_first_time = true;
var preview_button_pressed = false;

$(window).resize(function() {
	$('.dynamic_container,.dynamic_grid_1').height( $(document).height() );
});

/*
	update lasers
*/
function update_lasers() {
	
	if( !preview_button_pressed ) { return false; }
	
	var columns = $('.slider-columns .guide').slider('value');
	var gutter_percentage = $('.slider-percentage .guide').slider('value');
	var min_width = $('.slider-min-max .guide').slider('values', 0);
	var max_width = $('.slider-min-max .guide').slider('values', 1);
	
	var columns = $('.slider-columns .ruler ul li').eq(columns).html();
	var gutter_percentage = $('.slider-percentage .ruler ul li').eq(gutter_percentage).html();;
	var min_width = $('.slider-min-max .ruler ul li').eq(min_width).html();
	var max_width = $('.slider-min-max .ruler ul li').eq(max_width).html();
	if( isNaN(max_width) ) max_width = 'none';
	
	// console.log( max_width );
	
	lasers( gutter_percentage, columns, min_width, max_width );
}

/*
 initiate lasers
*/
function lasers( gutter_percentage, columns, min_width, max_width ) {
	grid( gutter_percentage, columns, min_width, max_width );

	$(window).trigger('resize');
	
	// install the lasers + beams.
	$('.dynamic_grid_1').each(function(n) {
		var laser = '<div class="laser"><img src="images/nozzle.png" width="42" height="33" alt="Laser" class="nozzle"><img src="images/beam.png" width="18" height="100%" alt="Beam" class="beam"></div>';
		
		var $laser_left = $(laser).addClass('laser-left');
		$laser_left.appendTo( $(this) );
		
		var $laser_right = $(laser).addClass('laser-right');
		$laser_right.appendTo( $(this) );
	})
	
	// lasers
	var lasers = $('.laser').length;
	$('.laser').each(function(n) {
		var $laser = $(this);
		var $nozzle = $(this).find('.nozzle');
		var $beam = $(this).find('.beam');
		
		// console.log( $nozzle.height() );
		
		$beam.css({
			'top': -1 * $beam.height(),
			'opacity': 0
		});
		
		if( lasers_first_time ) {
			$laser.css({
				'top': -1 * $nozzle.height()
			}).animate({
				'top': '0'
			});
		}
	});
	
	// console.log('lasers');
	
	show_lasers();
	
}

function show_lasers() {
	
	setTimeout(function() {
		$('.laser .beam').css({'top':0}).animate({'opacity':1});
	}, (lasers_first_time ? 700 : 0) );
	
	lasers_first_time = false;
}