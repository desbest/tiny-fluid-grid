$(document).ready(function() {
	$('.download').click(function() {
	
		var columns = $('.slider-columns .guide').slider('value');
		var gutter_percentage = $('.slider-percentage .guide').slider('value');
		var min_width = $('.slider-min-max .guide').slider('values', 0);
		var max_width = $('.slider-min-max .guide').slider('values', 1);
		
		var columns = $('.slider-columns .ruler ul li').eq(columns).html();
		var gutter_percentage = $('.slider-percentage .ruler ul li').eq(gutter_percentage).html();;
		var min_width = $('.slider-min-max .ruler ul li').eq(min_width).html();
		var max_width = $('.slider-min-max .ruler ul li').eq(max_width).html();
		
		window.location.href = 'download/build/' + columns + '_' + gutter_percentage + '_' + min_width + '_' + max_width + '.zip';
	
	});
});