/*
 & tinyfluidgrid.js
 */

var current_gutter_percentage = 20;
var current_grid_columns = 12;
var current_min_width = 800;
var current_max_width = 1400;

var grid_1_selector = '.dynamic_grid_1';
var container_selector = '.dynamic_container';

function grid( gutter_percentage, grid_columns, min_width, max_width ) {
	// var gutter_percentage = 40;
	// var grid_columns = 12;

	current_min_width = min_width;
	current_max_width = max_width;
	current_gutter_percentage = gutter_percentage;
	current_grid_columns = grid_columns;
	
	var container_width = $('.dynamic_container').width() / 100;
	
	var each_gutter_percent = gutter_percentage / ( grid_columns * 2 );
	var each_column_percent = ( 100 - gutter_percentage ) / grid_columns;
	
	var rounded_each_gutter_percent = each_gutter_percent.toString().substr(0,each_gutter_percent.toString().indexOf('.') + 2);
	var rounded_each_column_percent = each_column_percent.toString().substr(0,each_column_percent.toString().indexOf('.') + 2);
	
	var current_min_width = ( current_min_width * (rounded_each_gutter_percent / 100) * (current_grid_columns * 2 ) ) + ( current_min_width * (rounded_each_column_percent/100) * current_grid_columns );
	current_min_width = Math.floor( current_min_width );
	
	var current_max_width = ( current_max_width * (rounded_each_gutter_percent / 100) * (current_grid_columns * 2 ) ) + ( current_max_width * (rounded_each_column_percent/100) * current_grid_columns );
	current_max_width = Math.floor( current_max_width );
	
	// each_gutter_percent = Math.ceil( each_gutter_percent * 10 ) / 10;
	// each_column_percent = Math.ceil( each_column_percent * 10) / 10;
	
	var total_gutter_percent = each_gutter_percent * ( grid_columns * 2 );
	var total_column_percent = each_column_percent * grid_columns;
	
//	console.log( 'Gutter Percentage: ', gutter_percentage );
//	console.log( 'Grid Columns: ', grid_columns );
//	console.log( 'Each Gutter Percent: ', each_gutter_percent );
//	console.log( 'Each Column Percent: ', each_column_percent );
//	console.log( 'Total Gutter Percent: ', total_gutter_percent );
//	console.log( 'Total Column Percent: ', total_column_percent );
//	console.log( 'Minimum Width: ', current_min_width );
//	console.log( 'Maximum Width: ', current_max_width );
	
	$('.dynamic_container').css({
		'minWidth': current_min_width * ( ( total_gutter_percent / 100 ) + ( total_column_percent / 100 ) ) + 'px',
		'maxWidth': current_max_width * ( ( total_gutter_percent / 100 ) + ( total_column_percent / 100 ) ) + 'px',
		
		// 'minWidth': current_min_width,
		// 'maxWidth': ( isNaN(max_width) ? 'none' : current_max_width ),
		
		'margin': 'auto'
	});
	
	// $('.dynamic > *').remove();
	
	var number_of_current = $('.dynamic_container .dynamic_grid_1').length;
	
	if( number_of_current < grid_columns ) {
		for( var i = number_of_current; i<grid_columns; i++ ) {
			$('<div class="dynamic_grid_1"></div>').appendTo('.dynamic_container');
		}
	} else {
		// console.log( '.dynamic .dynamic_grid_1:gt(' + (number_of_current-1) + ')' );
		
		$('.dynamic_container .dynamic_grid_1:gt(' + (grid_columns-1) + ')').remove();
	}
	
	$('.dynamic_container .dynamic_grid_1').html('');
	
	for( var i = 1; i<=grid_columns; i++ ) {
		// console.log( '.grid_' + i );
		
		$('._grid_' + i + ', .dynamic_container .dynamic_grid_' + i).css({
			'marginLeft': each_gutter_percent + '%',
			'marginRight': each_gutter_percent + '%',
			'float': 'left',
			'display': 'block',
			
			'width': ( ( i * each_column_percent ) + ( ((i-1)*2) * each_gutter_percent ) ) + '%'
		});
		
	/*	
		$('.grid_' + i).css({
			'marginLeft': ( each_gutter_percent * container_width ) + 'px',
			'marginRight': ( each_gutter_percent * container_width ) + 'px',
			'float': 'left',
			'display': 'block',
			
			'width': ( container_width * ( ( i * each_column_percent ) + ( ((i-1)*2) * each_gutter_percent ) ) ) + 'px'
		});
	*/
	}
}